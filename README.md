# estudo-tdd

## Objetivo estudar teste unitário.

## Tipos de testes

![img](./test/images/piramede-testes.png)


Fluxo do teste unitário

1) Escreva um teste que falhe;
2) Faça o teste passe;
3) Refatore o código;
4) Repita o processo até terminar os testes.

![img](./test/images/fluxo_teste_unitario.jpg)

## BDD x TDD


![img](./test/images/BDD_versus_TDD.jpg)

. BDD - Behavior Driven Development
   É voltado para compreenção de pessoas leigas
   Ex.: expect(valor).toEqual(valorProposto)


. TDD - Test Driven Development
   É voltado para pessos técnicas
   Ex.: assertEqual(valor, valorProposto)

### Dependências

#### Bootstrap
 npm install --save-dev bootstrap

#### Jasmine js
npm install -g jasmine
npm install --save-dev jasmine
jasmine init

Documentação jasmine: https://www.npmjs.com/package/jasmine

#### Karma js
npm install -g karma
npm install --save-dev karma
npm install --save-dev karma-jasmine
npm install --save-dev karma-firefox-launcher

karma init karma.conf.js